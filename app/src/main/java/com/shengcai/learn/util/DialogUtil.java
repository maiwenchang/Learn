package com.shengcai.learn.util;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.shengcai.learn.R;

import java.util.List;

public class DialogUtil {

    // 展示提示信息
    public static void showToast(final Activity activity, final String text) {
        try {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    toast(activity, text);
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private static void toast(Activity activity, String msg) {
        if (activity == null)
            return;
        Toast toast = Toast.makeText(activity, msg, Toast.LENGTH_SHORT);
        toast.show();
    }
    /**
     * 展示弹窗(显示输入框)
     */
    public static Dialog showAlert(Context context, String title, String content, String hint, String rightText, String leftText, OnClickListener rightClick, OnClickListener leftClick) {
        Dialog alert = new Dialog(context, R.style.AlertDialog);
        try {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.dialog_alert, null);
            //标题
            TextView titleView = (TextView) view.findViewById(R.id.alert_title);
            if (TextUtils.isEmpty(title)) {
                titleView.setVisibility(View.GONE);
            } else {
                titleView.setText(title);
            }
            //提示语
            TextView contentView = (TextView) view.findViewById(R.id.alert_content);
            if (TextUtils.isEmpty(content)) {
                contentView.setVisibility(View.GONE);
            } else {
                contentView.setText(content);
            }
            //输入框提示语
            EditText editText = (EditText) view.findViewById(R.id.alert_content_edit);
            editText.setVisibility(View.VISIBLE);
            if (!TextUtils.isEmpty(hint)) {
                editText.setHint(hint);
            }
            //右按钮
            Button btnRight = (Button) view.findViewById(R.id.alert_yes);
            Button btnLeft = (Button) view.findViewById(R.id.alert_no);
            if (TextUtils.isEmpty(rightText)) {
                btnRight.setVisibility(View.GONE);
            } else {
                btnRight.setText(rightText);
                btnRight.setOnClickListener(rightClick);
            }
            //左按钮
            if (TextUtils.isEmpty(leftText)) {
                btnLeft.setVisibility(View.GONE);
            } else {
                btnLeft.setText(leftText);
                btnLeft.setOnClickListener(leftClick);
            }
            alert.setContentView(view);
            alert.setCanceledOnTouchOutside(true);
            alert.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return alert;
    }



    /**
     * 展示弹窗
     */
    public static Dialog showAlert(Context context, String title, String content, String rightText, String leftText, OnClickListener rightClick, OnClickListener leftClick) {
        Dialog alert = new Dialog(context, R.style.AlertDialog);
        try {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.dialog_alert, null);
            //标题
            TextView titleView = (TextView) view.findViewById(R.id.alert_title);
            if (TextUtils.isEmpty(title)) {
                titleView.setVisibility(View.GONE);
            } else {
                titleView.setText(title);
            }
            //提示语
            TextView contentView = (TextView) view.findViewById(R.id.alert_content);
            if (TextUtils.isEmpty(content)) {
                contentView.setVisibility(View.GONE);
            } else {
                contentView.setText(content);
            }
            //右按钮
            Button btnRight = (Button) view.findViewById(R.id.alert_yes);
            Button btnLeft = (Button) view.findViewById(R.id.alert_no);
            if (TextUtils.isEmpty(rightText)) {
                btnRight.setVisibility(View.GONE);
            } else {
                btnRight.setText(rightText);
                btnRight.setOnClickListener(rightClick);
            }
            //左按钮
            if (TextUtils.isEmpty(leftText)) {
                btnLeft.setVisibility(View.GONE);
            } else {
                btnLeft.setText(leftText);
                btnLeft.setOnClickListener(leftClick);
            }
            alert.setContentView(view);
            alert.setCanceledOnTouchOutside(true);
            alert.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return alert;
    }

    /**
     * 展示弹窗(可插入多个按钮)
     */
    public static Dialog showAlertCompound(Context context, String title, String content, String defaultText,
                                           OnClickListener defaultListener, List<String> btnList, List<OnClickListener> listeners) {

        Dialog alert = new Dialog(context, R.style.AlertDialog);
        try {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.dialog_alert2, null);
            //标题
            TextView titleView = (TextView) view.findViewById(R.id.alert_title);
            if (TextUtils.isEmpty(title)) {
                titleView.setVisibility(View.GONE);
            } else {
                titleView.setText(title);
            }
            //提示语
            TextView contentView = (TextView) view.findViewById(R.id.alert_content);
            if (TextUtils.isEmpty(content)) {
                contentView.setVisibility(View.GONE);
            } else {
                contentView.setText(content);
            }
            //取消按钮
            Button btnDefault = (Button) view.findViewById(R.id.btn_0);
            if (TextUtils.isEmpty(defaultText)) {
                btnDefault.setVisibility(View.GONE);
            } else {
                btnDefault.setText(defaultText);
                btnDefault.setOnClickListener(defaultListener);
            }
            if (btnList != null && listeners != null && btnList.size() == listeners.size()) {
                LinearLayout ll_options = (LinearLayout) view.findViewById(R.id.ll_options);
                for (int i = 0; i < btnList.size(); i++) {
                    //添加按钮
                    Button button = new Button(context);
                    button.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, DensityUtil.dip2px(context, 40)));
                    button.setBackgroundResource(R.drawable.item_selector);
                    button.setTextColor(0xFF555555);
                    button.setTextSize(TypedValue.COMPLEX_UNIT_SP, 17);
                    button.setText(btnList.get(i));
                    button.setOnClickListener(listeners.get(i));
                    ll_options.addView(button);
                    //添加分割线
                    View divider = new View(context);
                    divider.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, DensityUtil.dip2px(context, 0.5f)));
                    divider.setBackgroundColor(0xFFdddddd);
                    ll_options.addView(divider);
                }
            } else if (btnList != null && listeners != null) {
                Log.e(DialogUtil.class.getSimpleName(), "showAlertCompound: 按钮与监听事件数量不相等");
            }
            alert.setContentView(view);
            alert.setCanceledOnTouchOutside(true);
            alert.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return alert;
    }

    // 选择弹窗
    public static Dialog showOption(Activity mContext, @NonNull String text, OnClickListener noClick) {
        Dialog alert = new Dialog(mContext, R.style.OptionDialog);
        try {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.dialog_text, null);
            TextView tv_text = (TextView) view.findViewById(R.id.text);
            tv_text.setText(text);
            alert.setContentView(view);
            alert.setCancelable(true);
            alert.setCanceledOnTouchOutside(true);
            alert.show();
            tv_text.setOnClickListener(noClick);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return alert;
    }

}
