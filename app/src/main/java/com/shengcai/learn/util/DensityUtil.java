package com.shengcai.learn.util;

import android.content.Context;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;

/**
 * 屏幕单位计算类
 * 
 * @author MAIWENCHANG
 * on 2017/10/12.
 *
 */
public class DensityUtil {

	/**
	 * 根据手机的分辨率从 dp 的单位 转成为 px(像素)
	 */
	public static int dip2px(Context context, float dpValue) {
		float scale = context.getResources().getDisplayMetrics().density;
		return (int) (dpValue * scale + 0.5f);
	}

	/**
	 * 根据手机的分辨率从 px(像素) 的单位 转成为 dp
	 */
	public static int px2dip(Context context, float pxValue) {
		float scale = context.getResources().getDisplayMetrics().density;
		return (int) (pxValue / scale + 0.5f);
	}

	public static boolean isPad(Context context) {
		if(context == null)
			return false;
		try {
			WindowManager wm = (WindowManager) context
					.getSystemService(Context.WINDOW_SERVICE);
			Display display = wm.getDefaultDisplay();
			// 屏幕宽度
			float screenWidth = display.getWidth();
			// 屏幕高度
			float screenHeight = display.getHeight();
			DisplayMetrics dm = new DisplayMetrics();
			display.getMetrics(dm);
			double x = Math.pow(dm.widthPixels / dm.xdpi, 2);
			double y = Math.pow(dm.heightPixels / dm.ydpi, 2);
			// 屏幕尺寸
			double screenInches = Math.sqrt(x + y);
			// 大于6尺寸则为Pad
			if (screenInches >= 6.44) {
				return true;
			}
			return false;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

}
