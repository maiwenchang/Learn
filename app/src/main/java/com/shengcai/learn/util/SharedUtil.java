package com.shengcai.learn.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.StringRes;

import com.shengcai.learn.base.SCApplication;

import java.util.Map;

/**
 * 缓存管理工具
 * Created by MaiWenChang on 2017/10/12.
 */

public class SharedUtil {

    /*通用数据*/
    public static final String COMMON_DATA = "common_data";

    private static SharedPreferences sharedPreferences = SCApplication.appContext.getSharedPreferences(COMMON_DATA, Context.MODE_PRIVATE);
    private static SharedPreferences.Editor editor = sharedPreferences.edit();;

    public SharedUtil (String preferenceName){
        try {
            sharedPreferences = SCApplication.appContext.getSharedPreferences(preferenceName, Context.MODE_PRIVATE);
            editor = sharedPreferences.edit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void put(@StringRes int resid, Object value) {
        try {
            String key = SCApplication.appContext.getResources().getString(resid);
            put(key, value);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String getString(@StringRes int resid, String defValue) {
        try {
            String key = SCApplication.appContext.getResources().getString(resid);
            return sharedPreferences.getString(key, defValue);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return defValue;
    }

    public static int getInt(@StringRes int resid, int defValue) {
        try {
            String key = SCApplication.appContext.getResources().getString(resid);
            return sharedPreferences.getInt(key, defValue);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return defValue;
    }


    public static boolean getBoolean(@StringRes int resid, boolean defValue) {
        try {
            String key = SCApplication.appContext.getResources().getString(resid);
            return sharedPreferences.getBoolean(key, defValue);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return defValue;
    }


    public static float getFloat(@StringRes int resid, float defValue) {
        try {
            String key = SCApplication.appContext.getResources().getString(resid);
            return sharedPreferences.getFloat(key, defValue);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return defValue;
    }

    public static long getLong(@StringRes int resid, long defValue) {
        try {
            String key = SCApplication.appContext.getResources().getString(resid);
            return sharedPreferences.getLong(key, defValue);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return defValue;
    }

    /**
     * 保存数据的方法，拿到数据保存数据的基本类型，然后根据类型调用不同的保存方法
     *
     * @param key
     * @param object
     */
    public static void put(String key, Object object) {
        try {
            if (object instanceof String) {
                editor.putString(key, (String) object);
            } else if (object instanceof Integer) {
                editor.putInt(key, (Integer) object);
            } else if (object instanceof Boolean) {
                editor.putBoolean(key, (Boolean) object);
            } else if (object instanceof Float) {
                editor.putFloat(key, (Float) object);
            } else if (object instanceof Long) {
                editor.putLong(key, (Long) object);
            } else {
                editor.putString(key, object.toString());
            }
            editor.apply();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 获取保存数据的方法，我们根据默认值的到保存的数据的具体类型，然后调用相对于的方法获取值
     *
     * @param key           键的值
     * @param defaultObject 默认值
     * @retrn
     */
    private static Object get(String key, Object defaultObject) {
        try {
            if (defaultObject instanceof String) {
                return sharedPreferences.getString(key, (String) defaultObject);
            } else if (defaultObject instanceof Integer) {
                return sharedPreferences.getInt(key, (Integer) defaultObject);
            } else if (defaultObject instanceof Boolean) {
                return sharedPreferences.getBoolean(key, (Boolean) defaultObject);
            } else if (defaultObject instanceof Float) {
                return sharedPreferences.getFloat(key, (Float) defaultObject);
            } else if (defaultObject instanceof Long) {
                return sharedPreferences.getLong(key, (Long) defaultObject);
            } else {
                return sharedPreferences.getString(key, null);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return defaultObject;
    }

    /**
     * 移除某个key值已经对应的值
     *
     * @param key
     */
    public static void remove(String key) {
        try {
            editor.remove(key);
            editor.apply();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 清除所有的数据
     */
    public static void clear() {
        try {
            editor.clear();
            editor.apply();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 查询某个key是否存在
     *
     * @param key
     * @return
     */
    public static boolean contains(String key) {
        return sharedPreferences.contains(key);
    }

    /**
     * 返回所有的键值对
     *
     * @return
     */
    public static Map<String, ?> getAll() {
        return sharedPreferences.getAll();
    }

}
