package com.shengcai.learn.util;

import android.content.pm.ApplicationInfo;

import com.shengcai.learn.base.SCApplication;

/**
 * Created by MaiWenChang on 2017/10/12.
 */

public class AppUtil {

    /*是否调试模式*/
    private static Boolean isDebug = null;
    public static boolean isDebug() {/*是否调试模式*/
        if (isDebug == null) {
            isDebug = SCApplication.appContext.getApplicationInfo() != null
                    && (SCApplication.appContext.getApplicationInfo().flags
                    & ApplicationInfo.FLAG_DEBUGGABLE) != 0;
        }
        return isDebug ;
    }



}
