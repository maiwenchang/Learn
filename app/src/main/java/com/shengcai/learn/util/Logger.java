package com.shengcai.learn.util;

import android.util.Log;

/**
 * Log工具类
 * Created by MaiWenChang on 2017/10/12.
 */

public class Logger {

    private static String className;//类名
    private static String methodName;//方法名
    private static int lineNumber;//行数

    private static String createLog( String log ) {
        return methodName + "(" + className + ":" + lineNumber + "): " + log;
    }

    private static void getMethodNames(StackTraceElement[] sElements){
        className = sElements[1].getFileName();
        methodName = sElements[1].getMethodName();
        lineNumber = sElements[1].getLineNumber();
    }


    public static void e(String message){
        if (AppUtil.isDebug()) {
            // Throwable instance must be created before any methods
            getMethodNames(new Throwable().getStackTrace());
            Log.e(className, createLog(message));
        }
    }


    public static void i(String message){
        if (AppUtil.isDebug()) {
            getMethodNames(new Throwable().getStackTrace());
            Log.i(className, createLog(message));
        }
    }

    public static void d(String message){
        if (AppUtil.isDebug()) {
            getMethodNames(new Throwable().getStackTrace());
            Log.d(className, createLog(message));
        }
    }

    public static void v(String message){
        if (AppUtil.isDebug()) {
            getMethodNames(new Throwable().getStackTrace());
            Log.v(className, createLog(message));
        }

    }

    public static void w(String message){
        if (AppUtil.isDebug()) {
            getMethodNames(new Throwable().getStackTrace());
            Log.w(className, createLog(message));
        }
    }

    public static void wtf(String message){
        if (AppUtil.isDebug()) {
            getMethodNames(new Throwable().getStackTrace());
            Log.wtf(className, createLog(message));
        }
    }

}
