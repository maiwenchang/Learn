package com.shengcai.learn.constant;

import android.os.Environment;

public class Constants {

    public static final int MSG_FROM_CLIENT = 0;
    public static final int MSG_FROM_SERVICE = 1;

}
