package com.shengcai.learn.base;

import android.content.Context;
import android.content.Intent;

/**
 * Builder方式启动Activity
 * 把需要的参数写进builder,可以添加Flag
 *
 * Created by MaiWenChang on 2017/11/4.
 */

public abstract class BaseRequestBuilder implements IRequestBuilder {

    @Override
    public void startActivity(Context context) {
        Intent intent = build(context);
        context.startActivity(intent);
    }

    @Override
    public void startActivity(Context context, int intentFlag) {
        Intent intent = build(context);
        intent.setFlags(intentFlag);
        context.startActivity(intent);
    }
}
