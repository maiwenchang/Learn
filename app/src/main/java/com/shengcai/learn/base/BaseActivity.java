package com.shengcai.learn.base;

import android.os.Bundle;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

/**
 *
 * Created by MaiWenChang on 2017/10/11.
 */

public abstract class BaseActivity extends AppCompatActivity implements IBaseInterface{

    private UIHandler mHandler = new UIHandler(Looper.getMainLooper());

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //初始化流程
        getExtraData();
        setContentView();
        initView();
        initData();
    }

    /**
     * 获取启动参数
     */
    protected void getExtraData() {

    }

    protected abstract void setContentView();

    /**
     * 初始化视图
     */
    protected abstract void initView();

    /*
    * 初始化数据
    */
    protected abstract void initData();

    /**
     * 需要处理Message队列时可调用此方法得到Handler，如果不需要处理消息，可以直接调用getHandler()；
     * @param handler IHandler
     * @return UIHandler
     */
    protected UIHandler getHandler(IHandler handler) {
        mHandler.setHandler(handler);
        return mHandler;
    }

    protected UIHandler getHandler() {
        return mHandler;
    }


}
