package com.shengcai.learn.base;

import android.app.Application;
import android.content.Context;

import com.shengcai.learn.aidl.BinderPool;
import com.shengcai.learn.aidl.BinderPool2;
import com.shengcai.learn.net.retrofit.BaseNetProvider;
import com.shengcai.learn.net.retrofit.NetMgr;

/**
 * Created by MaiWenChang on 2017/10/12.
 */

public class SCApplication extends Application {

    public static Context appContext;

    @Override
    public void onCreate() {
        super.onCreate();
        appContext = getApplicationContext();

        //注册网络请求框架
        NetMgr.getInstance().registerProvider(new BaseNetProvider());

        //启动服务池
        startServices();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        appContext = this;
    }

    /**
     * 启动服务池
     */
    public void startServices(){
        new Thread(new Runnable() {

            @Override
            public void run() {
                BinderPool.getInstance(appContext);
                //BinderPool2.Companion.getInstance(appContext);
            }
        }).start();
    }
}
