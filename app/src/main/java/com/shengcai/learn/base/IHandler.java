package com.shengcai.learn.base;

import android.os.Message;

/**
 * Created by 2850537913 on 2017/11/4.
 */

public interface IHandler {

    void handleMessage(Message msg);

}
