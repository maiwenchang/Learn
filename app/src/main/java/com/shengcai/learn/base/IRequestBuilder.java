package com.shengcai.learn.base;

import android.content.Context;
import android.content.Intent;

/**
 * Created by MaiWenChang on 2017/11/4.
 */

public interface IRequestBuilder {

    Intent build(Context context);

    void startActivity(Context context);

    void startActivity(Context context,int intentFlag);

}
