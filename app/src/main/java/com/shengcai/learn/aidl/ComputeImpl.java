package com.shengcai.learn.aidl;

import android.os.RemoteException;

import com.shengcai.learn.aidl.ICompute;

/**
 * Created by 2850537913 on 2018/1/18.
 */

public class ComputeImpl extends ICompute.Stub {

    private static int i = 1;

    @Override
    public int factorial(int n) throws RemoteException {
        return factorialOperation(n);
    }


    @Override
    public int getI() throws RemoteException {
//        try {
//            Thread.sleep(1000);//模拟耗时操作,阻塞Service中的主线程
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
        return i;
    }

    @Override
    public void startLoop() throws RemoteException {
        new Thread(new Runnable() {
            @Override
            public void run() {//在Serverce中开启子线程
                for (int i = 0; i < 100; i++) {
                    try {
                        ComputeImpl.i = i;
                        Thread.sleep(1000);
                        if (i == 99) i = 0;
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }

    private int factorialOperation(int n) {
        if (n == 0) {
            return 1;
        } else if (n == 1) {
            return 1;
        } else {
            return n * factorialOperation(n - 1);
        }
    }
}
