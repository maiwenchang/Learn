package com.shengcai.learn.aidl

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.IBinder
import com.shengcai.learn.aidl.BinderPoolService2.Companion.BINDER_COMPUTE
import com.shengcai.learn.util.Logger
import java.util.concurrent.CountDownLatch

/**
 * Created by 2850537913 on 2018/1/20.
 */
class BinderPool2 private constructor(private val mContext: Context) {

    init {
        connectBinderPoolService()
    }

    companion object {

        @Volatile
        @JvmStatic
        var instance: BinderPool2? = null

        fun getInstance(context: Context): BinderPool2 {//单例模式
            if (instance == null) {
                synchronized(BinderPool2::class.java) {
                    if (instance == null) {
                        instance = BinderPool2(context.applicationContext)
                    }
                }

            }
            return instance!!
        }
    }

    private var mCountDownLatch: CountDownLatch? = null
    private var mBinderPool: IBinderPool2? = null

    fun queryBinder(binderCode: Int): IBinder {
        return mBinderPool?.queryBinder(binderCode)!!
    }

    private fun connectBinderPoolService() {
        mCountDownLatch = CountDownLatch(1)
        val intent = Intent(mContext, BinderPoolService2::class.java)

        mContext.bindService(intent, object : ServiceConnection {
            override fun onServiceDisconnected(name: ComponentName?) {
            }

            override fun onServiceConnected(name: ComponentName?, service: IBinder?) {
                mBinderPool = IBinderPool2.Stub.asInterface(service)

                mBinderPool?.asBinder()?.linkToDeath(object : IBinder.DeathRecipient {
                    override fun binderDied() {
                        Logger.i("binder died.")
                        mBinderPool?.asBinder()?.unlinkToDeath(this, 0)
                        mBinderPool = null
                        connectBinderPoolService()
                    }
                }, 0)

                mCountDownLatch?.countDown()
            }
        }, Context.BIND_AUTO_CREATE)
        mCountDownLatch!!.await()

        val iBinder = mBinderPool?.queryBinder(BINDER_COMPUTE)
        ComputeImpl.asInterface(iBinder)?.startLoop()

    }

}