package com.shengcai.learn.aidl

import android.app.Service
import android.content.Intent
import android.os.IBinder
import com.shengcai.learn.util.Logger

/**
 * Created by 2850537913 on 2018/1/20.
 */
class BinderPoolService2 : Service() {

    companion object {
        @JvmStatic
        val BINDER_COMPUTE = 0
        @JvmStatic
        val BINDER_SECURITY_CENTER = 1
    }

    override fun onCreate() {
        Logger.i("BinderPoolService2 : "+"onCreate")
        super.onCreate()
    }

    override fun onDestroy() {
        Logger.i("BinderPoolService2 : "+"onDestroy")
        super.onDestroy()
    }

    override fun onBind(intent: Intent?): IBinder {
        Logger.i("BinderPoolService2 : "+"onBind")
        return binderPoolImpl2
    }

    private val binderPoolImpl2 = object : IBinderPool2.Stub() {

        override fun queryBinder(binderCode: Int): IBinder {
            val binder: IBinder?
            when (binderCode) {
                BINDER_COMPUTE -> binder = ComputeImpl()
                BINDER_SECURITY_CENTER -> binder = null
                else -> binder = null
            }
            return binder!!
        }

    }
}