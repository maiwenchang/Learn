package com.shengcai.learn.module.home.view;

import android.app.Fragment;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.shengcai.learn.R;
import com.shengcai.learn.net.base.ApiService;
import com.shengcai.learn.net.base.BaseUrl;
import com.shengcai.learn.net.base.URL;
import com.shengcai.learn.util.Logger;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * 首页
 * Created by MaiWenChang on 2017/10/12.
 *
 * @author MaiWenChang
 */
public class MainFragment extends Fragment {

    private TextView tv;
    private ImageView iv;
    private ApiService apiService = ApiService.getInstance();

    public static MainFragment newInstance(String param1) {
        MainFragment fragment = new MainFragment();
        Bundle args = new Bundle();
        args.putString("agrs1", param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        Bundle bundle = getArguments();
        String agrs1 = bundle.getString("agrs1");
        iv = (ImageView) view.findViewById(R.id.iv);
        tv = (TextView) view.findViewById(R.id.tv);
        tv.setText(agrs1);
        Map<String, String> params = new HashMap<>();
        apiService.get(BaseUrl.guolin, URL.GetCity, params)
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                        try {
                            ResponseBody body = response.body();
                            if (body == null) return;
                            String s = body.string();
                            if (TextUtils.isEmpty(s)) return;
                            tv.setText(tv.getText().toString() + ": " + s);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                        Logger.wtf(t.getMessage());
                    }
                });

        Map<String, String> params2 = new HashMap<>();
        params2.put("method", "List");
        params2.put("isAppShow", "1");
        apiService.post(BaseUrl.app100, URL.Bulletin, params2)
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {

                    }

                    @Override
                    public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {

                    }
                });
//        Glide.with(getActivity())
//                .load("http://file.100xuexi.com/EbookMaterial/Animation/2017090119241952.gif")
//                .asGif()
//                .into(iv);
        Glide.with(getActivity())
                .load("")
                .asGif()
                .into(iv);
        return view;
    }
}
