package com.shengcai.learn.module.customlayout

import com.shengcai.learn.R
import com.shengcai.learn.base.BaseActivity

/**
 * 粘性滚动的ScrollView
 * Created by Wenchang Mai on 2018/2/23.
 */
class ScrollViewTestActivity : BaseActivity() {
    override fun setContentView() {
        setContentView(R.layout.activity_scroll_view_test)
    }

    override fun initView() {

    }

    override fun initData() {

    }
}