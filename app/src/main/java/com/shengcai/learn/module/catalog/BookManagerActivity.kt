package com.shengcai.learn.module.catalog

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.Bundle
import android.os.IBinder
import com.shengcai.learn.base.BaseActivity
import com.shengcai.learn.util.Logger
import org.jetbrains.anko.*
import org.jetbrains.anko.verticalLayout

/**
 * Created by 2850537913 on 2018/1/20.
 */
class BookManagerActivity:BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }


    override fun setContentView() {

    }

    private var iBookManager: IBookManager? = null

    override fun initView() {
        verticalLayout {
            padding = dip(20)
            val edit_text = editText{
                hint = "输入书名"
            }
            button("添加"){
                setOnClickListener {
                    val bookName = edit_text.text.toString()
                    doAsync {
                        iBookManager?.addBook(Book(3,bookName))
                    }
                }
            }
            button("开启服务"){
                setOnClickListener {
                    val intent = Intent(context,BookManagerService::class.java)
                    bindService(intent,connection, Context.BIND_AUTO_CREATE)
                }
            }

        }
    }

    val connection = object : ServiceConnection{
        override fun onServiceDisconnected(name: ComponentName?) {
            val intent = Intent(applicationContext,BookManagerService::class.java)
            bindService(intent,this,Context.BIND_AUTO_CREATE)
        }

        override fun onServiceConnected(name: ComponentName?, service: IBinder?) {
            Logger.i("server connected")
            iBookManager = IBookManager.Stub.asInterface(service)
            doAsync {
                iBookManager?.addBook(Book(1, "Android"))
                iBookManager?.addBook(Book(2, "Ios"))
                val bookList = iBookManager?.getBookList()
                bookList?.forEach{
                    Logger.i(it.toString())
                }
                iBookManager?.registerListener(onNewBookArrivedListener)
            }
        }
    }

    val onNewBookArrivedListener = object :IOnNewBookArrivedListener.Stub(){
        override fun onNewBookArrived(newBook: Book?) {
            Logger.i(newBook.toString())
        }
    }

    override fun initData() {

    }

    override fun onDestroy() {
        super.onDestroy()
        doAsync {
            iBookManager?.unregisterListener(onNewBookArrivedListener)
            unbindService(connection)
        }
    }
}