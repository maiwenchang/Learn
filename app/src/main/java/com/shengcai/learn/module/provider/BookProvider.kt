package com.shengcai.learn.module.provider

import android.content.ContentProvider
import android.content.ContentValues
import android.content.UriMatcher
import android.database.Cursor
import android.net.Uri
import android.util.Log

/**
 *
 * Created by maiwenchang on 2018/2/8.
 */
class BookProvider : ContentProvider() {

    companion object {

        val AUTHORITY = "com.shengcai.learn.book.provider"
        val BOOK_URI_CODE = 0
        val USER_URI_CODE = 1
        val sUriMatcher = UriMatcher(UriMatcher.NO_MATCH)

        val BOOK_CONTENT_URI = Uri.parse("content://"+AUTHORITY+"/book")
        val USER_CONTENT_URI = Uri.parse("content://"+AUTHORITY+"//user")

        init {
            sUriMatcher.addURI(AUTHORITY, "book", BOOK_URI_CODE)
            sUriMatcher.addURI(AUTHORITY, "user", USER_URI_CODE)
        }

        fun getTableName(uri: Uri?) = when (sUriMatcher.match(uri)) {
                BOOK_URI_CODE -> MySqlHelper.BOOK_TABLE_NAME
                USER_URI_CODE -> MySqlHelper.USER_TABLE_NAME
                else -> null
        }

    }

    override fun insert(uri: Uri?, values: ContentValues?): Uri? {
        Log.d("","insert")
        val table = getTableName(uri) ?: throw IllegalArgumentException("Unsuported URI: $uri")
        //val row = getContext().database.writableDatabase.insert(table, null, values)
        val row = context.database.use {
            insert(table, null, values)
        }
        if (row > 0) {
            context.contentResolver.notifyChange(uri, null)
        }
        return uri
    }

    override fun query(uri: Uri?, projection: Array<out String>?, selection: String?, selectionArgs: Array<out String>?, sortOrder: String?): Cursor {
        Log.d("","query")
        val table = getTableName(uri) ?: throw IllegalArgumentException("Unsuported URI: $uri")

        return  context
                .database
                .readableDatabase
                .query(table, projection, selection, selectionArgs, null, null, sortOrder, null)
    }

    override fun onCreate(): Boolean {
        Log.d("","current thread: ${Thread.currentThread().name}")
        return true
    }

    override fun update(uri: Uri?, values: ContentValues?, selection: String?, selectionArgs: Array<out String>?): Int {
        Log.d("","update")
        val table = getTableName(uri) ?: throw IllegalArgumentException("Unsupported URI: $uri")
        val row = context.database.use {
            update(table, values, selection, selectionArgs)
        }
        if (row > 0) {
            context.contentResolver.notifyChange(uri, null)
        }
        return row
    }

    override fun delete(uri: Uri?, selection: String?, selectionArgs: Array<out String>?): Int {
        Log.d("","delete")
        val table = getTableName(uri) ?: throw IllegalArgumentException("Unsupported URI: $uri")
        val row = context.database.use {
            delete(table, selection, selectionArgs)
        }
        if (row > 0) {
            context.contentResolver.notifyChange(uri, null)
        }
        return row
    }

    override fun getType(uri: Uri?): String? {
        Log.d("","getType")
        return null
    }
}