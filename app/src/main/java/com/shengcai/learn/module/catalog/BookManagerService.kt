package com.shengcai.learn.module.catalog

import android.app.Service
import android.content.Intent
import android.content.pm.PackageManager
import android.os.IBinder
import android.os.Parcel
import android.os.RemoteCallbackList
import android.os.RemoteException
import android.util.Log
import com.shengcai.learn.util.Logger
import java.util.concurrent.CopyOnWriteArrayList

/**
 * Created by 2850537913 on 2018/1/20.
 */
class BookManagerService : Service() {

    private val mBookList = CopyOnWriteArrayList<Book>()
    private val mListenerList = RemoteCallbackList<IOnNewBookArrivedListener>()

    override fun onCreate() {
        super.onCreate()
    }

    override fun onBind(intent: Intent?): IBinder {
        return mBinder;
    }

    val mBinder = object : IBookManager.Stub() {

        @Throws(RemoteException::class)
        override fun onTransact(code: Int, data: Parcel, reply: Parcel, flags: Int): Boolean {
            //验证开启服务权限
            val check = checkCallingOrSelfPermission("com.shengcai.learn.permission.ACCESS_BOOK_SERVICE")
            Logger.d("check=" + check)
            if (check == PackageManager.PERMISSION_DENIED) {
                return false
            }

            //验证服务请求者
            var packageName: String? = null
            val packages = packageManager.getPackagesForUid(getCallingUid())
            if (packages != null && packages.size > 0) {
                packageName = packages[0]
            }
            Logger.d("onTransact: " + packageName!!)
            return if (!packageName.startsWith("com.shengcai")) {
                false
            } else super.onTransact(code, data, reply, flags)
        }

        override fun getBookList(): MutableList<Book> {
            return mBookList
        }


        override fun addBook(book: Book?) {
            mBookList.add(book)
            val N = mListenerList.beginBroadcast() - 1
            for (i in 0..N) {
                try {
                    mListenerList.getBroadcastItem(i)?.onNewBookArrived(book)
                } catch (e: Exception) { }
            }
            mListenerList.finishBroadcast()
        }

        override fun registerListener(listener: IOnNewBookArrivedListener?) {
            mListenerList.register(listener)
        }

        override fun unregisterListener(listener: IOnNewBookArrivedListener?) {
            mListenerList.unregister(listener)
        }

    }
}