package com.shengcai.learn.module.provider

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import org.jetbrains.anko.db.*

/**
 *
 * Created by maiwenchang on 2018/2/8.
 */

class MySqlHelper(val context:Context):ManagedSQLiteOpenHelper(context,"mydb"){

    companion object {

        val BOOK_TABLE_NAME = "book"
        val USER_TABLE_NAME = "user"

        private var instance: MySqlHelper? = null

        @Synchronized
        fun getInstance(context: Context):MySqlHelper {
            if (instance == null) {
                instance = MySqlHelper(context.applicationContext)
            }
            return instance!!
        }
    }

    override fun onCreate(db: SQLiteDatabase?) {
        db?.createTable(BOOK_TABLE_NAME,true,"_id" to INTEGER + PRIMARY_KEY + AUTOINCREMENT,"name" to TEXT)
        db?.createTable(USER_TABLE_NAME,true,"_id" to INTEGER + PRIMARY_KEY + AUTOINCREMENT,"name" to TEXT,"sex" to INTEGER)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        //To change body of created functions use File | Settings | File Templates.
    }

}

val Context.database:MySqlHelper
    get() = MySqlHelper.getInstance(applicationContext)