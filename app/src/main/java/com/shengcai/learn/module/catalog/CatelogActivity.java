package com.shengcai.learn.module.catalog;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.annotation.Nullable;
import android.view.View;

import com.shengcai.learn.aidl.BinderPool2;
import com.shengcai.learn.aidl.BinderPoolService2;
import com.shengcai.learn.aidl.ICompute;
import com.shengcai.learn.R;
import com.shengcai.learn.aidl.BinderPool;
import com.shengcai.learn.aidl.ComputeImpl;
import com.shengcai.learn.base.BaseActivity;
import com.shengcai.learn.util.DialogUtil;
import com.shengcai.learn.util.Logger;

/**
 * Created by 2850537913 on 2018/1/16.
 */

public class CatelogActivity extends BaseActivity {
    private ICompute computer;

    @Override
    protected void setContentView() {
        setContentView(R.layout.layout_second_activity);
    }

    @Override
    protected void initView() {
        findViewById(R.id.btn_start).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent = new Intent(CatelogActivity.this, ThirdActivity.class);
//                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                startActivity(intent);
                Intent intent = new Intent("me.cv.ktpractice.f");
                intent.setData(Uri.parse("ktpractice://me.cv:80/aaa"));
                startActivity(intent);
            }
        });
        findViewById(R.id.get_i).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            int i = computer.getI();
                            Logger.i("i: "+String.valueOf(i));
                            DialogUtil.showToast(CatelogActivity.this,String.valueOf(i));
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                }).start();
            }
        });
    }

    @Override
    protected void initData() {
        IBinder iBinder = BinderPool2.Companion.getInstance(CatelogActivity.this).queryBinder(BinderPoolService2.getBINDER_COMPUTE());
        computer = ComputeImpl.asInterface(iBinder);
        new Thread(new Runnable() {
            @Override
            public void run() {
                compute();
            }
        }).start();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Logger.i("");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Logger.i("");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Logger.i("");
    }

    private void compute(){
        try {
            int factorial = computer.factorial(4);
            Logger.i(String.valueOf(factorial));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
