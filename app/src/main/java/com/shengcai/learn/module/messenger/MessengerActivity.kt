package com.shengcai.learn.module.messenger

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.*
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import com.shengcai.learn.base.BaseActivity
import com.shengcai.learn.constant.Constants
import com.shengcai.learn.module.catalog.BookManagerActivity
import com.shengcai.learn.util.Logger
import org.jetbrains.anko.*

/**
 * Created by 2850537913 on 2018/1/19.
 */
public class MessengerActivity : BaseActivity() {

    private object MessengerHandler: Handler() {
        override fun handleMessage(msg: Message?) {
            super.handleMessage(msg)
            when (msg?.what) {
                Constants.MSG_FROM_SERVICE -> Logger.i("receive msg from Service:" + msg.getData().getString("reply")!!)
                else -> super.handleMessage(msg)
            }
        }
    }

    var mMessenger: Messenger? = Messenger(MessengerHandler)
    var serviceMessenger: Messenger? = null

    override fun setContentView() {
        verticalLayout {
            padding = dip(16)
            val text = editText {
                hint = "请输入内容"
                textSize = 24f
            }
            button("发送") {
                setOnClickListener {
                    val message = Message()
                    message.what = Constants.MSG_FROM_CLIENT
                    message.replyTo = mMessenger
                    message.data = bundleOf(Pair("msg", text.text.trim().toString()))
                    serviceMessenger?.send(message)
                }
            }
            button("绑定") {
                setOnClickListener {
                    val intent = Intent(context,MessengerService::class.java)
                    bindService(intent, object : ServiceConnection{
                        override fun onServiceDisconnected(name: ComponentName?) {
                        }

                        override fun onServiceConnected(name: ComponentName?, service: IBinder?) {
                            serviceMessenger = Messenger(service)
                            val message = Message()
                            message.what = Constants.MSG_FROM_CLIENT
                            message.data = bundleOf(Pair("msg", "hello, this is client."))
                            message.replyTo = mMessenger
                            serviceMessenger?.send(message)
                        }
                    }, Context.BIND_AUTO_CREATE)
                }
            }

            button("BookManagerActivity"){
                setOnClickListener {
                    startActivity<BookManagerActivity>()
                }
            }
        }
    }


    override fun initView() {
        val divider: DividerItemDecoration  = DividerItemDecoration(this,LinearLayoutManager.VERTICAL)

    }

    override fun initData() {

    }


}