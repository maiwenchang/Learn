package com.shengcai.learn.module.exercise.view;


import com.shengcai.learn.module.exercise.bean.PaperNodeQuestionBean;

import java.util.List;

public interface IPaperView {

    void loadData(int index);

    /**
     * 自动保存做题记录在本地缓存（每做一题）,
     * 正常退出时，删除此记录
     */
    void autoSaveProgress(final String questionId, final String tikuName);

    /*** 设置主标题*/
    void setTopTitle(String topTitle);

    /***弹出右上角菜单*/
    void showMorePopupWindow();

    void insertQuestionList(List<PaperNodeQuestionBean> questionList);

    void getPaperCardData(int position);

    void setPaperCardView(int position);

    //PaperView getPaperView();

    /****设置标记图标状态*/
    void setMarkIconState(String state);

    /****设置标记图标状态*/
    void setCollectIconState(String state);

    /**
     * 设置上一题，下一题图标
     **/
    void setJumpButtonState(int index);

    /**
     * 设置paperView的位置
     **/
    void resetPaperView(int index);

    void videoFullPlay(String path, long currentPosition);
}
