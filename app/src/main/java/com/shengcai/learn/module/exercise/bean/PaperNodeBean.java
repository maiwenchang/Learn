package com.shengcai.learn.module.exercise.bean;

import java.io.Serializable;
import java.util.List;

/**
 * 试卷的大题
 * <p>
 * Created by Administrator on 2017/4/23.
 */

public class PaperNodeBean implements Serializable {

    /*===========以下字段对应数据库(sctk_update.db/tb_E_ExamPaperNode)============**/

    /*** 试卷ID**/
    protected String paperID;

    /*** 试卷大题的ID**/
    private String paperNodeID;

    /*** 题型名称，类似 "A1/A2题型"**/
    private String paperNodeName;

    /*** 题型说明，类似 " 以下每一道考题下面有A、B、C、D、E五个备选答案......"**/
    private String paperNodeDesc;

    /*** 题型ID(tb_Dic_QuestionType中有对应关系)**/
    private String questionTypeID;

    private int listOrder;

    /*** 试题列表 **/
    protected List<PaperNodeQuestionBean> questionList;

     /*===========补充字段==========**/

    /***题库ID*/
    private String tiKuID;

    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    public String getPaperID() {
        return paperID;
    }

    public void setPaperID(String paperID) {
        this.paperID = paperID;
    }

    public String getPaperNodeID() {
        return paperNodeID;
    }

    public void setPaperNodeID(String paperNodeID) {
        this.paperNodeID = paperNodeID;
    }

    public String getPaperNodeName() {
        return paperNodeName;
    }

    public void setPaperNodeName(String paperNodeName) {
        this.paperNodeName = paperNodeName;
    }

    public String getPaperNodeDesc() {
        return paperNodeDesc;
    }

    public void setPaperNodeDesc(String paperNodeDesc) {
        this.paperNodeDesc = paperNodeDesc;
    }

    public String getQuestionTypeID() {
        return questionTypeID;
    }

    public void setQuestionTypeID(String questionTypeID) {
        this.questionTypeID = questionTypeID;
    }

    public int getListOrder() {
        return listOrder;
    }

    public void setListOrder(int listOrder) {
        this.listOrder = listOrder;
    }

    public List<PaperNodeQuestionBean> getQuestionList() {
        return questionList;
    }

    public void setQuestionList(List<PaperNodeQuestionBean> questionList) {
        this.questionList = questionList;
    }

    public String getTiKuID() {
        return tiKuID;
    }

    public void setTiKuID(String tiKuID) {
        this.tiKuID = tiKuID;
    }
}
