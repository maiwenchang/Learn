package com.shengcai.learn.module.exercise.bean;

import java.io.Serializable;
import java.util.List;

/**
 * 试卷数据封装
 *
 * Created by Administrator on 2017/4/21.
 */

public class PaperBean implements Serializable {

    /*
     * 以下字段对应数据库(sctk_update.db/tb_E_ExamPaper)
     */

    /*** 试卷ID**/
    private String paperID;

    /*** 试卷名称*/
    private String paperName;

    /***试卷说明*/
    private String desc;

    /***试卷中的大题模块*/
    private List<PaperNodeBean> nodeList;


    /*
     * 补充字段
     */

    /*** 题库ID**/
    private String tiKuID;

    /*** 最后一次做题的试题ID*/
    private String latestQuestionID;

    /*** 试卷类型*/
    private String paperType;

    /*** 购买状态*/
    private String isBuy;

    /*** 可查看的答案数量*/
    private String allowAnswerCount;

    /*** 可查看的视频数量*/
    private String allowVideoCount;

    /*** 开始时间*/
    private String startTime;

    /*** 结束时间*/
    private String endTime;

    /*** 错误数量*/
    private int errorCount;

    /*** 收藏数量*/
    private int collCount;

    /*** 将获得总分数*/
    private String totalPoint = "0";

    /*** 做题进度*/
    private String progressIndex;

    /*** 流水ID*/
    private String UGID;

    /*** 试题数量**/
    private String count;

    /*** 题库价格**/
    private String TkPrice;

    /*** 题库名称**/
    private String TkName;

    /*** 题库试题数量**/
    private String TkQuestionCount;

    /*** 题库封面地址**/
    private String TkCoverImg = "";

    /***做题用时*/
    private String consumeTime;

    /***试卷分类名称，如：[章节题库，历年真题]*/
    private String chapterName;

    /*试卷排序：0默认排序1顺序排序2整卷排序*/
    private int testsSort;

    /***==================================================================================*/

    public String getPaperID() {
        return paperID;
    }

    public void setPaperID(String paperID) {
        this.paperID = paperID;
    }

    public String getPaperName() {
        return paperName;
    }

    public void setPaperName(String paperName) {
        this.paperName = paperName;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public void setNodeList(List<PaperNodeBean> nodeList) {
        this.nodeList = nodeList;
    }

    public List<PaperNodeBean> getNodeList() {
        return nodeList;
    }

    /***==================================================================================*/

    public String getTiKuID() {
        return tiKuID;
    }

    public void setTiKuID(String tiKuID) {
        this.tiKuID = tiKuID;
    }

    public String getPaperType() {
        return paperType;
    }

    public void setPaperType(String paperType) {
        this.paperType = paperType;
    }

    public String getIsBuy() {
        return isBuy;
    }

    public void setIsBuy(String isBuy) {
        this.isBuy = isBuy;
    }

    public String getAllowAnswerCount() {
        return allowAnswerCount;
    }

    public void setAllowAnswerCount(String allowAnswerCount) {
        this.allowAnswerCount = allowAnswerCount;
    }

    public String getAllowVideoCount() {
        return allowVideoCount;
    }

    public void setAllowVideoCount(String allowVideoCount) {
        this.allowVideoCount = allowVideoCount;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public int getErrorCount() {
        return errorCount;
    }

    public void setErrorCount(int errorCount) {
        this.errorCount = errorCount;
    }

    public int getCollCount() {
        return collCount;
    }

    public void setCollCount(int collCount) {
        this.collCount = collCount;
    }

    public String getTotalPoint() {
        return totalPoint;
    }

    public void setTotalPoint(String totalPoint) {
        this.totalPoint = totalPoint;
    }

    public String getProgressIndex() {
        return progressIndex;
    }

    public void setProgressIndex(String progressIndex) {
        this.progressIndex = progressIndex;
    }

    public String getUGID() {
        return UGID;
    }

    public void setUGID(String UGID) {
        this.UGID = UGID;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getLatestQuestionID() {
        return latestQuestionID;
    }

    public void setLatestQuestionID(String latestQuestionID) {
        this.latestQuestionID = latestQuestionID;
    }

    public String getTkPrice() {
        return TkPrice;
    }

    public void setTkPrice(String tkPrice) {
        TkPrice = tkPrice;
    }

    public String getTkName() {
        return TkName;
    }

    public void setTkName(String tkName) {
        TkName = tkName;
    }

    public String getTkQuestionCount() {
        return TkQuestionCount;
    }

    public void setTkQuestionCount(String tkQuestionCount) {
        TkQuestionCount = tkQuestionCount;
    }

    public String getTkCoverImg() {
        return TkCoverImg;
    }

    public void setTkCoverImg(String tkCoverImg) {
        TkCoverImg = tkCoverImg;
    }

    public String getConsumeTime() {
        return consumeTime;
    }

    public void setConsumeTime(long consumeTime) {
        this.consumeTime = String.valueOf(consumeTime);
    }

    public String getChapterName() {
        return chapterName;
    }

    public void setChapterName(String chapterName) {
        this.chapterName = chapterName;
    }

    public void setTestsSort(int testsSort) {
        this.testsSort = testsSort;
    }

    public int getTestsSort() {
        return testsSort;
    }
}
