package com.shengcai.learn.module.exercise.presenter;

import android.os.Bundle;
import android.widget.CheckBox;

import com.shengcai.learn.module.exercise.bean.PaperBean;
import com.shengcai.learn.module.exercise.bean.PaperNodeQuestionBean;
import com.shengcai.learn.module.exercise.model.IPaperModel;

import java.util.List;

/**
 *
 * Created by Administrator on 2017/4/21.
 */

public interface IPaperPresenter {

    /***初始化Model*/
    void setModel(IPaperModel paperModel);

    /**设置题库相关信息，如价格等**/
    void setTkInfo(Bundle bundle);

    /***加载数据
     * @param index*/
    void loadData(int index);

    /***把试卷中所有大题的试题放到一个列表中*/
    List<PaperNodeQuestionBean> listQuestion(PaperBean paperBean);

    void setQuestionDetail(PaperNodeQuestionBean paperBean, int position);

    /*** 下一题*/
    void onNextClick();

    /*** 上一题*/
    void onLastClick();

    /*** 返回*/
    void onBackClick();

    /*** 更多*/
    void onMoreClick();

    /*** 收藏*/
    void onCollectClick(int index, CheckBox doCollectCb);

    /**** 标记*/
    void onMarkClick(int index);

    /***点击答题卡*/
    void onAnswerCardClick();

    /***设置标记状态*/
    void setMarkState(int index);

    /***设置收藏状态*/
    void setCollectState(int index);

    /***用户选择答案*/
    void onUserSelectAnswer(int index);

    /***重置试卷*/
    void onResetPaperClick();

    /***卷面设置*/
    void onSetPaperClick();

    /***纠错
     * @param index*/
    void onFeedBackClick(int index);

    /***处理做题记录*/
    void handlePaperRecord(int index);

    /***获取购买权限*/
    String getIsBuy(int index);

    /***跳转到登录*/
    void goLogin();

    /***跳转到购买*/
    void goBuy();

    /***处理上下一题按钮显示*/
    void setJumpButtonState(int index);

    /***保存试卷信息到数据库*/
    void savePaperInfo();

    /**下一试卷ID**/
    String getLastPaperID();

    /**上一试卷ID**/
    String getNextPaperID();

    /**获取试卷所在类似[章节题库]的目录名称**/
    String getChapterName();


    void videoFullPlay(String path, long currentPosition);
}
