package com.shengcai.learn.module.home.view;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.FloatingActionButton;
import android.view.MenuItem;
import android.view.View;

import com.shengcai.learn.R;
import com.shengcai.learn.base.BaseActivity;
import com.shengcai.learn.base.BaseRequestBuilder;
import com.shengcai.learn.base.IHandler;
import com.shengcai.learn.base.UIHandler;
import com.shengcai.learn.module.catalog.CatelogActivity;
import com.shengcai.learn.util.BottomNavigationViewHelper;
import com.shengcai.learn.util.Logger;

/**
 * 主页Activity
 * Created by MaiWenChang on 2017/10/11.
 */
public class MainActivity extends BaseActivity {

    public final static String ARG1 = "arg1";
    public final static String ARG2 = "arg2";
    private FragmentManager fm;

    // views
    private MainFragment mainFragment;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onPause() {
        super.onPause();
        Logger.i("");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Logger.i("");
    }

    @Override
    protected void setContentView() {
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void initView() {
        //底部控制栏
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        BottomNavigationViewHelper.disableShiftMode(navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        fm = this.getFragmentManager();
        mainFragment = MainFragment.newInstance(getResources().getString(R.string.title_home));
        fm.beginTransaction().replace(R.id.tb, mainFragment).commit();
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(MainActivity.this,CatelogActivity.class);
                //intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });
        findViewById(R.id.fab2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(MainActivity.this,ThirdActivity.class);
                //intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });


    }

    @Override
    protected void initData() {
        getHandler().post(new Runnable() {
            @Override
            public void run() {

            }
        });
    }

    private UIHandler mHandler = getHandler(new IHandler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    break;

            }
        }
    });

    public static RequestBuilder build() {
        return new RequestBuilder();
    }

    public static class RequestBuilder extends BaseRequestBuilder {

        private String arg1;
        private String arg2;

        public RequestBuilder arg1(String arg1) {
            this.arg1 = arg1;
            return this;
        }

        public RequestBuilder arg2(String arg2) {
            this.arg2 = arg2;
            return this;
        }

        @Override
        public Intent build(Context context) {
            Intent intent = new Intent(context, MainActivity.class);
            intent.putExtra(ARG1, arg1);
            intent.putExtra(ARG2, arg2);
            return intent;
        }
    }

    @Override
    protected void getExtraData() {
        super.getExtraData();
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            String arg1 = bundle.getString(ARG1);
            String arg2 = bundle.getString(ARG2);
            Logger.d("arg1: " + arg1 + ", arg2: " + arg2);
        }
    }

    /**
     * 底部控制栏按钮点击事件
     */
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            //开启事务
            FragmentTransaction transaction = fm.beginTransaction();
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    mainFragment = MainFragment.newInstance(getResources().getString(R.string.title_home));
                    transaction.replace(R.id.tb, mainFragment).commit();
                    return true;
                case R.id.catalog:
                    mainFragment = MainFragment.newInstance(getResources().getString(R.string.title_catalog));
                    transaction.replace(R.id.tb, mainFragment).commit();
                    return true;
                case R.id.me:
                    mainFragment = MainFragment.newInstance(getResources().getString(R.string.title_me));
                    transaction.replace(R.id.tb, mainFragment).commit();
                    return true;
                case R.id.bookshelf:
                    mainFragment = MainFragment.newInstance(getResources().getString(R.string.title_bookshelf));
                    transaction.replace(R.id.tb, mainFragment).commit();
                    return true;
                default:
                    break;
            }
            return false;
        }

    };

}
