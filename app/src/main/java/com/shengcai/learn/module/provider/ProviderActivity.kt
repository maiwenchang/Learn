package com.shengcai.learn.module.provider

import android.content.ContentValues
import android.database.ContentObserver
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.widget.LinearLayout
import com.shengcai.learn.base.BaseActivity
import com.shengcai.learn.util.Logger
import org.jetbrains.anko.*
import org.jetbrains.anko.db.asMapSequence

/**
 * Created by maiwenchang on 2018/2/8.
 */
class ProviderActivity : BaseActivity(){

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


    }

    override fun setContentView() {
    }

    private var bookLayout: LinearLayout? = null

    override fun initView() {
        verticalLayout {

            val edit_text = editText{
                hint = "输入书名"
            }

            button("添加"){
                setOnClickListener {
                    val bookName = edit_text.text.toString()
                    val values = ContentValues()
                    values.put("name",bookName)
                    contentResolver.insert(BookProvider.BOOK_CONTENT_URI, values)
                }
            }

            scrollView {
                bookLayout = verticalLayout {

                }
            }

        }
    }



    override fun initData() {
        contentResolver.registerContentObserver(BookProvider.BOOK_CONTENT_URI,false,MyBookCO())
        contentResolver.registerContentObserver(BookProvider.USER_CONTENT_URI,false,MyBookCO())
        val bookCursor = contentResolver.query(BookProvider.BOOK_CONTENT_URI, arrayOf("_id", "name"), null, null, null)
        bookCursor?.asMapSequence()?.iterator()?.forEach {
            Logger.d("query book: id ${it.get("_id")}, name ${it.get("name")}")
        }
        bookCursor?.close()
        val userCursor = contentResolver.query(BookProvider.USER_CONTENT_URI, arrayOf("_id", "name", "sex"), null, null, null)
        userCursor?.asMapSequence()?.iterator()?.forEach {
            Logger.d("query user: id ${it.get("_id")}, name ${it.get("name")}")
        }
        userCursor?.close()
    }

    inner class MyBookCO : ContentObserver(Handler(Looper.getMainLooper())) {

        override fun onChange(selfChange: Boolean, uri: Uri?) {
            val table = BookProvider.getTableName(uri)
            when (table) {
                MySqlHelper.BOOK_TABLE_NAME -> {
                    Logger.d("onChange :${MySqlHelper.BOOK_TABLE_NAME}")
                    val bookCursor =  contentResolver.query(BookProvider.BOOK_CONTENT_URI, arrayOf("_id", "name"), null, null, null)
                    bookCursor?.asMapSequence()?.iterator()?.forEach {
                        Logger.d("query book: id ${it.get("_id")}, name ${it.get("name")}")
                    }
                    bookCursor?.close()
                }
                MySqlHelper.USER_TABLE_NAME -> Logger.d("onChange :${MySqlHelper.USER_TABLE_NAME}")
            }
            super.onChange(selfChange, uri)
        }
    }

}