package com.shengcai.learn.module.home.view

import android.content.Intent
import com.shengcai.learn.R
import com.shengcai.learn.aidl.BinderPool
import com.shengcai.learn.aidl.ComputeImpl
import com.shengcai.learn.base.BaseActivity
import com.shengcai.learn.module.customlayout.ScrollViewTestActivity
import com.shengcai.learn.module.messenger.MessengerActivity
import com.shengcai.learn.module.provider.ProviderActivity
import com.shengcai.learn.util.DialogUtil
import com.shengcai.learn.util.Logger
import org.jetbrains.anko.button
import org.jetbrains.anko.verticalLayout


/**
 * Created by maiwenchang on 2018/1/17.
 */

class ThirdActivity : BaseActivity() {


    override fun setContentView() {
        setContentView(R.layout.layout_third_activity)
    }

    override fun initView() {
        verticalLayout {

            button("启动MainActivity") {

                setOnClickListener {
                    val intent = Intent(this@ThirdActivity, MainActivity::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT)
                    startActivity(intent)
                }
            }

            button("getI") {

                setOnClickListener {
                    Thread(Runnable {
                        val iBinder = BinderPool.getInstance(this@ThirdActivity).queryBinder(BinderPool.BINDER_COMPUTE)
                        val compute = ComputeImpl.asInterface(iBinder)
                        try {
                            val i = compute.i
                            DialogUtil.showToast(this@ThirdActivity, i.toString())
                            Logger.i("i: " + i.toString())
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }).start()
                }
            }

            button("MessengerActivity") {

                setOnClickListener {
                    //Intent intent = new Intent("com.shengcai.learn.MessengerActivity");
                    val intent = Intent(this@ThirdActivity, MessengerActivity::class.java)
                    startActivity(intent)
                }
            }

            button("ProviderActivity") {

                setOnClickListener {
                    //Intent intent = new Intent("com.shengcai.learn.MessengerActivity");
                    val intent = Intent(this@ThirdActivity, ProviderActivity::class.java)
                    startActivity(intent)
                }
            }

            button("ScrollViewActivity") {

                setOnClickListener {
                    //Intent intent = new Intent("com.shengcai.learn.MessengerActivity");
                    val intent = Intent(this@ThirdActivity, ScrollViewTestActivity::class.java)
                    startActivity(intent)
                }
            }

            button("shutdown") {

                setOnClickListener {
                    //Intent intent = new Intent("com.shengcai.learn.MessengerActivity");
                    Logger.v("root Runtime->shutdown")
                    //shutdown()
                    //Process proc =Runtime.getRuntime().exec(new String[]{"su","-c","shutdown"});  //关机
                    //val proc = Runtime.getRuntime().exec(arrayOf("su", "-c", "reboot"))  //重启
                    val proc = Runtime.getRuntime().exec(arrayOf("su", "-c", "reboot -p"))  //关机
                    proc.waitFor()
                }
            }

        }

    }

    override fun initData() {

    }
}
