package com.shengcai.learn.module.exercise.model;


import com.shengcai.learn.module.exercise.bean.PaperBean;
import com.shengcai.learn.module.exercise.bean.PaperNodeQuestionBean;

import java.util.List;

/**
 *
 * Created by Administrator on 2017/4/22.
 */

public interface IPaperModel {

    /***请求试卷*/
    interface LoadPaperListener {
        void onFinish(PaperBean paperBean);
        void onError(String errorMessage);
    }

    /***请求试题*/
    interface LoadQuestionListener{
        void onFinish(PaperNodeQuestionBean questionBean);
        void onError(String errorMessage);
    }

    interface ModelCallback{
        void onFinish();
        void onError();
    }

    /***获取做题进度*/
    int getLastIndex();

    /***重做*/
    void onResetPaperClick();

    /***擦除做题记录（包括标记状态）*/
    void releaseQuestionRecord(PaperBean paperBean);

    /***隐藏用户答案（不重置标记状态）*/
    void hideUserAnswers(PaperBean paperBean);

    void SubmitRecord();

    /***请求试卷数据*/
    void requestPaperBean(LoadPaperListener listener);

    /***展开试题列表*/
    List<PaperNodeQuestionBean> getQuestionList(PaperBean paperBean);

    /***加载试题详细信息*/
    void getQuestionDetail(PaperNodeQuestionBean questionBean, LoadQuestionListener listener);

    /***获取试题的做题记录*/
    void getQuestionRecord(PaperBean paperBean);

    /**用户选择了答案*/
    void onUserSelectAnswer(int index);

    /***处理做题进度*/
    void handlePaperRecord(int index);

    /***标记*/
    void setMark(int index);

    /***添加收藏*/
    void addCollect(int index);

    /***取消收藏*/
    void deleteCollect(int index);

    /***查询收藏流水ID并取消收藏*/
    void existsCollection(int index, boolean delete);

    /***获取是做题的权限*/
    String getIsBuy(int index);

    /***获取可免费做题和观看视频的数量*/
    void getFreeCount();

    /***保存试卷信息到数据库*/
    void savePaperInfo();

    /**获取上一份试卷ID**/
    String getLastPaperID();

    /**获取下一份试卷ID**/
    String getNextPaperID();

    //========Getter and Setter=======================

    void setQuestionList(List<PaperNodeQuestionBean> questionList);

    List<PaperNodeQuestionBean> getQuestionList();

    void setPaperBean(PaperBean paperBean);

    PaperBean getPaperBean();

    int getPaperAnswerNum();

    void setPaperAnswerNum(int paperAnswerNum);

    int getAllowVideoNum();

    void setAllowVideoNum(int allowVideoNum);

    String getTiKuID();

    String getPaperID();

    void setMenuManageButtonID(String menuManageButtonID);

    String getMenuManageButtonID();

    String getTkName();

    void setTkName(String tkName);

    String getTkCoverImg();

    void setTkCoverImg(String tkCoverImg);

    String getQuestionCount();

    void setQuestionCount(String questionCount);

    String getPrice();

    void setPrice(String price);

    void setChapterName(String chapterName);

    String getChapterName();

}
