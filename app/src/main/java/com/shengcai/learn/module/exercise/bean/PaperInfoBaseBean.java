package com.shengcai.learn.module.exercise.bean;

import java.io.Serializable;

public class PaperInfoBaseBean implements Serializable {
    /**
     * 题库ID
     **/
    protected String tiku_id;
    /**
     * 试卷ID
     **/
    protected String paperID;
    /**
     * 试卷类型
     */
    protected String chapterType;
    /**
     * 试卷名称
     */
    protected String paperName;
    /**
     * 章节类型名称
     */
    protected String chapterTypeName;
    protected String chapterTag;
    /**
     * 题目类型
     */
    protected String typeName;
    /**
     * 中文类型名称
     **/
    protected String typeNameTow;
    /**
     * 可查看的答案数量
     */
    protected String paperAnserNum;
    /**
     * 可查看的视频数量
     */
    protected String allowVideoNum;
    /**
     * 购买状态
     */
    protected String isBuy;
    /**
     * 开始时间
     */
    protected String startTime;
    /**
     * 结束时间
     */
    protected String endTime;
    /**
     * 错误数量
     */
    protected int errCount;
    /**
     * 标题
     */
    protected String title = "";
    /**
     * 总时间
     */
    protected String time = "";
    /**
     * 收藏数量
     */
    protected int collCount;
    /**
     * 将获得总分数
     */
    protected String totalPoint = "0";
    /**
     * 试题数量
     **/
    protected String count;
    /**
     * 做题进度
     */
    protected String progressIndex;

    protected String index;

    protected String ugid;

    public String getTiku_id() {
        return tiku_id;
    }

    public void setTiku_id(String tiku_id) {
        this.tiku_id = tiku_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPaperID() {
        return paperID;
    }

    public void setPaperID(String paperID) {
        this.paperID = paperID;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getChapterType() {
        return chapterType;
    }

    public void setChapterType(String chapterType) {
        this.chapterType = chapterType;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getPaperName() {
        return paperName;
    }

    public void setPaperName(String paperName) {
        this.paperName = paperName;
    }

    public String getChapterTypeName() {
        return chapterTypeName;
    }

    public void setChapterTypeName(String chapterTypeName) {
        this.chapterTypeName = chapterTypeName;
    }

    public String getPaperAnserNum() {
        return paperAnserNum;
    }

    public void setPaperAnserNum(String paperAnserNum) {
        this.paperAnserNum = paperAnserNum;
    }

    public String getAllowVideoNum() {
        return allowVideoNum;
    }

    public void setAllowVideoNum(String allowVideoNum) {
        this.allowVideoNum = allowVideoNum;
    }

    public String getIsBuy() {
        return isBuy;
    }

    public void setIsBuy(String isBuy) {
        this.isBuy = isBuy;
    }

    public String getChapterTag() {
        return chapterTag;
    }

    public void setChapterTag(String chapterTag) {
        this.chapterTag = chapterTag;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public String getTypeNameTow() {
        return typeNameTow;
    }

    public void setTypeNameTow(String typeNameTow) {
        this.typeNameTow = typeNameTow;
    }

    public int getErrCount() {
        return errCount;
    }

    public void setErrCount(int errCount) {
        this.errCount = errCount;
    }

    public int getCollCount() {
        return collCount;
    }

    public void setCollCount(int collCount) {
        this.collCount = collCount;
    }

    public String getProgressIndex() {
        return progressIndex;
    }

    public void setProgressIndex(String progressIndex) {
        this.progressIndex = progressIndex;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public String getUgid() {
        return ugid;
    }

    public void setUgid(String ugid) {
        this.ugid = ugid;
    }

    public String getTotalPoint() {
        return totalPoint;
    }

    public void setTotalPoint(String totalPoint) {
        this.totalPoint = totalPoint;
    }
}
