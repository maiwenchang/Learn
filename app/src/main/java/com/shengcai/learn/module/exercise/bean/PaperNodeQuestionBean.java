package com.shengcai.learn.module.exercise.bean;

import java.io.Serializable;

/**
 * 试卷大题中的小题
 * <p>
 * Created by Administrator on 2017/4/21.
 */

public class PaperNodeQuestionBean implements Serializable {

    /***  以下字段对应数据库(sctk_update.db/tb_E_ExamPaperNodeQuestion和tb_E_Question)*/
    private String questionID; // 试题Id

    /***  以下字段对应数据库(sctk_update.db/tb_E_ExamPaperNodeQuestion)*/

    private String paperID; // 试卷ID
    private String paperNodeID;//大题ID
    private String paperName; // 试卷名称
    private String paperQuestionScore;//试题分值

    /***  以下字段对应数据库(sctk_update.db/tb_E_Question)*/

    private String questionCode;// 题号，形如EE174770
    private String PID; // 共用题干id
    private String questionTypeID; // 题型识别ID
    private String questionBaseTypeCode; // 题型名称，类似"single"
    private String questionScore;//试题分值
    private String answerNum;//选项数量
    private String selectAnswer; // 选项
    private String selectAnswerScore; // 选项分值
    private String questionContent; // 题目内容
    private String questionAnalysis; // 答案解析
    private String standarAnswer;//标准答案
    private String videoPath; // 视频地址
    private String videoTitle; // 视频标题
    private String videoImagePath; // 视频图片地址
    private int listOrder;// 排序，从1开始，包括组合题的子试题

    /*** 补充字段*/

    private String tiKuID;//题库ID
    private String userAnswer; // 用户的答案
    private String relativeKnowledge;//相关考点，需要从接口获取，数据库中没有相关的字段
    private String composeQuestionParent; // 组合题公共题干或共用备选答案
    private boolean isCompose;//是否组合题
    //知识点（考点）的 URL
    private String KNOWLEDGE_URL = "http://centerp.100xuexi.com/Topper/ProductAbout/KnowledgePointDetail.aspx?action=knowledge_list&display=text&ver=3&back=no&name=";

    private String paperNodeName;//题型名称,类似“A1/A2题型”
    private String paperNodeDesc;//题型说明，类似“以下每一道考题下面有A、B、C、D、E五个备选答案......”
    private int paperNodeListOrder;//大题的序号
    /**做题记录相关*/
    private String state = "0"; // 试题状态,0未做未标记，1已做未标记，2未做已标记，3已做已标记
    private String isRight = "0"; // 是否正确，0未做，1正确，-1错误
    private String isCollect = "0";//是否收藏
    private String collectUgID = "";//收藏流水ID
    private String collectTime;//收藏时间
    private String isSubmitRec;//记录是否需要提交: "0"已传, "1"需要上传;
    private String startTime;//开始做题的时间
    protected String endTime;//结束做题的时间
    private int composeListOrder;//组合题公共部分的序号

    private boolean multiChooseHasShowDaAn;    // 多选题是否点击过答案

    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    public String getQuestionID() {
        return questionID;
    }

    public void setQuestionID(String questionID) {
        this.questionID = questionID;
    }

    public String getPaperID() {
        return paperID;
    }

    public void setPaperID(String paperID) {
        this.paperID = paperID;
    }

    public String getPaperNodeID() {
        return paperNodeID;
    }

    public void setPaperNodeID(String paperNodeID) {
        this.paperNodeID = paperNodeID;
    }

    public String getPaperName() {
        return paperName;
    }

    public void setPaperName(String paperName) {
        this.paperName = paperName;
    }

    public String getPaperQuestionScore() {
        return paperQuestionScore;
    }

    public void setPaperQuestionScore(String paperQuestionScore) {
        this.paperQuestionScore = paperQuestionScore;
    }

    public String getQuestionCode() {
        return questionCode;
    }

    public void setQuestionCode(String questionCode) {
        this.questionCode = questionCode;
    }

    public String getPID() {
        return PID;
    }

    public void setPID(String PID) {
        this.PID = PID;
    }

    public String getQuestionTypeID() {
        return questionTypeID;
    }

    public void setQuestionTypeID(String questionTypeID) {
        this.questionTypeID = questionTypeID;
    }

    public String getQuestionBaseTypeCode() {
        return questionBaseTypeCode;
    }

    public void setQuestionBaseTypeCode(String questionBaseTypeCode) {
        this.questionBaseTypeCode = questionBaseTypeCode;
    }

    public String getQuestionScore() {
        return questionScore;
    }

    public void setQuestionScore(String questionScore) {
        this.questionScore = questionScore;
    }

    public String getAnswerNum() {
        return answerNum;
    }

    public void setAnswerNum(String answerNum) {
        this.answerNum = answerNum;
    }

    public String getSelectAnswer() {
        return selectAnswer;
    }

    public void setSelectAnswer(String selectAnswer) {
        this.selectAnswer = selectAnswer;
    }

    public String getSelectAnswerScore() {
        return selectAnswerScore;
    }

    public void setSelectAnswerScore(String selectAnswerScore) {
        this.selectAnswerScore = selectAnswerScore;
    }

    public String getQuestionContent() {
        return questionContent;
    }

    public void setQuestionContent(String questionContent) {
        this.questionContent = questionContent;
    }

    public String getQuestionAnalysis() {
        return questionAnalysis;
    }

    public void setQuestionAnalysis(String questionAnalysis) {
        this.questionAnalysis = questionAnalysis;
    }

    public String getStandarAnswer() {
        return standarAnswer;
    }

    public void setStandarAnswer(String standarAnswer) {
        this.standarAnswer = standarAnswer;
    }

    public String getVideoPath() {
        return videoPath;
    }

    public void setVideoPath(String videoPath) {
        this.videoPath = videoPath;
    }

    public String getVideoTitle() {
        return videoTitle;
    }

    public void setVideoTitle(String videoTitle) {
        this.videoTitle = videoTitle;
    }

    public String getVideoImagePath() {
        return videoImagePath;
    }

    public void setVideoImagePath(String videoImagePath) {
        this.videoImagePath = videoImagePath;
    }

    public String getUserAnswer() {
        return userAnswer;
    }

    public void setUserAnswer(String userAnswer) {
        this.userAnswer = userAnswer;
    }

    public int getListOrder() {
        return listOrder;
    }

    public void setListOrder(int listOrder) {
        this.listOrder = listOrder;
    }

    public String getComposeQuestionParent() {
        return composeQuestionParent;
    }

    public void setComposeQuestionParent(String composeQuestionParent) {
        this.composeQuestionParent = composeQuestionParent;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getIsRight() {
        return isRight;
    }

    public void setIsRight(String isRight) {
        this.isRight = isRight;
    }

    public boolean isMultiChooseHasShowDaAn() {
        return multiChooseHasShowDaAn;
    }

    public void setMultiChooseHasShowDaAn(boolean multiChooseHasShowDaAn) {
        this.multiChooseHasShowDaAn = multiChooseHasShowDaAn;
    }

    public String getPaperNodeName() {
        return paperNodeName;
    }

    public void setPaperNodeName(String paperNodeName) {
        this.paperNodeName = paperNodeName;
    }

    public String getPaperNodeDesc() {
        return paperNodeDesc;
    }

    public void setPaperNodeDesc(String paperNodeDesc) {
        this.paperNodeDesc = paperNodeDesc;
    }

    public String getRelativeKnowledge() {
        return relativeKnowledge;
    }

    public void setRelativeKnowledge(String relativeKnowledge) {
        this.relativeKnowledge = relativeKnowledge;
    }

    public int getPaperNodeListOrder() {
        return paperNodeListOrder;
    }

    public void setPaperNodeListOrder(int paperNodeListOrder) {
        this.paperNodeListOrder = paperNodeListOrder;
    }

    public boolean isCompose() {
        return isCompose;
    }

    public void setCompose(boolean compose) {
        isCompose = compose;
    }

    public String getKNOWLEDGE_URL() {
        return KNOWLEDGE_URL;
    }

    public void setKNOWLEDGE_URL(String KNOWLEDGE_URL) {
        this.KNOWLEDGE_URL = KNOWLEDGE_URL;
    }

    public String getIsCollect() {
        return isCollect;
    }

    public void setIsCollect(String isCollect) {
        this.isCollect = isCollect;
    }

    public String getCollectUgID() {
        return collectUgID;
    }

    public void setCollectUgID(String collectUgID) {
        this.collectUgID = collectUgID;
    }

    public String getCollectTime() {
        return collectTime;
    }

    public void setCollectTime(String collectTime) {
        this.collectTime = collectTime;
    }

    public String getIsSubmitRec() {
        return isSubmitRec;
    }

    public void setIsSubmitRec(String isSubmitRec) {
        this.isSubmitRec = isSubmitRec;
    }

    public String getTiKuID() {
        return tiKuID;
    }

    public void setTiKuID(String tiKuID) {
        this.tiKuID = tiKuID;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public void setComposeListOrder(int composeListOrder) {
        this.composeListOrder = composeListOrder;
    }

    public int getComposeListOrder() {
        return composeListOrder;
    }
}
