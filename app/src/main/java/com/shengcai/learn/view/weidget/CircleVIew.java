package com.shengcai.learn.view.weidget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.annotation.ColorInt;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

import com.shengcai.learn.R;
import com.shengcai.learn.util.DensityUtil;

/**
 * Created by MaiWenChang on 2017/10/26.
 */

public class CircleVIew extends View {

    private int mColor = Color.BLACK;                               //填充颜色
    private Paint mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);        //画笔

    public CircleVIew(Context context) {
        super(context);
    }

    public CircleVIew(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CircleVIew(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        TypedArray attr = context.obtainStyledAttributes(attrs, R.styleable.CircleView);
        mColor = attr.getColor(R.styleable.CircleView_color, Color.BLACK);//获取布局中配置的填充颜色
        attr.recycle();
    }

    /**
     * 设置颜色
     * @param color  ColorInt
     */
    public void setColor(@ColorInt int color) {
        mColor = color;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int wMode = MeasureSpec.getMode(widthMeasureSpec);
        int wSize = MeasureSpec.getSize(widthMeasureSpec);
        int hMode = MeasureSpec.getMode(heightMeasureSpec);
        int hSize = MeasureSpec.getSize(heightMeasureSpec);
        //默认宽高
        int mWidth = DensityUtil.dip2px(getContext(), 50) + getPaddingLeft() + getPaddingRight();
        int mHeight = DensityUtil.dip2px(getContext(), 50) + getPaddingTop() + getPaddingBottom();
        //处理wrap_content的情况
        if (wMode == MeasureSpec.AT_MOST && hMode == MeasureSpec.AT_MOST) {
            setMeasuredDimension(Math.min(mWidth, wSize), Math.min(mHeight, hSize));
        } else if (wMode == MeasureSpec.AT_MOST) {
            setMeasuredDimension(Math.min(mWidth, wSize), hSize);
        } else if (hMode == MeasureSpec.AT_MOST) {
            setMeasuredDimension(wSize, Math.min(mHeight, hSize));
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        int width = getWidth() - getPaddingLeft() - getPaddingRight();
        int height = getHeight() - getPaddingTop() - getPaddingBottom();
        int radius = Math.min(width, height) / 2;
        mPaint.setColor(mColor);
        canvas.drawCircle(getPaddingLeft() + width / 2, getPaddingTop() + height / 2, radius, mPaint);
    }


}
