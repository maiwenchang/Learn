package com.shengcai.learn.view.weidget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Matrix;
import android.graphics.Shader;
import android.support.annotation.Nullable;
import android.text.TextPaint;
import android.util.AttributeSet;

/**
 * 颜色渐变的TextView
 * Created by Wenchang Mai on 2018/2/23.
 */

public class ColorfulTextView extends android.support.v7.widget.AppCompatTextView {
    private int mViewWidth = 0;
    private float mTranslate = 0;
    private TextPaint mPaint;
    private LinearGradient mLinearGradient;
    private Matrix mMatrix;

    public ColorfulTextView(Context context) {
        super(context);
    }

    public ColorfulTextView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public ColorfulTextView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        if (mViewWidth == 0) {
            mViewWidth = getMeasuredWidth();
            if (mViewWidth > 0) {
                mPaint = getPaint();
                mLinearGradient = new LinearGradient(0,
                        0,
                        mViewWidth,
                        0,
                        new int[]{Color.BLUE, 0xFFffffff, Color.GREEN},
                        null,
                        Shader.TileMode.CLAMP);
                mPaint.setShader(mLinearGradient);
                mMatrix = new Matrix();
            }
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (mMatrix != null) {
            mTranslate += 2 * Math.PI / 25;
            if (mTranslate > 2 * Math.PI) {
                mTranslate = 0f;
            }
            mMatrix.setTranslate((float) (mViewWidth * Math.sin(mTranslate)),0);
            mLinearGradient.setLocalMatrix(mMatrix);
            postInvalidateDelayed(100);
        }
    }
}
