package com.shengcai.learn.view.layout;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

import com.shengcai.learn.R;

/**
 * Created by Wenchang Mai on 2018/2/22.
 */

public class MyTopBar extends RelativeLayout{
    public MyTopBar(Context context) {
        super(context);
    }

    public MyTopBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public MyTopBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.MyTopBar);
        String title = typedArray.getString(R.styleable.MyTopBar_title);
        int titleTextColor = typedArray.getColor(R.styleable.MyTopBar_titleTextColor, 0);
        typedArray.recycle();
    }




}
