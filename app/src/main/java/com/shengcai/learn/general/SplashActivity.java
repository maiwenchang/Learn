package com.shengcai.learn.general;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.shengcai.learn.module.home.view.MainActivity;

/**
 * App启动引导页
 * Created by MaiWenChang on 2017/10/12.
 */

public class SplashActivity extends Activity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MainActivity.build()
                .arg1("aaa")
                .arg2("bbb")
                .startActivity(this,Intent.FLAG_ACTIVITY_CLEAR_TOP);
        //startActivity(new Intent(this, MainActivity.class));
        overridePendingTransition(0,0);
        finish();

    }

}
