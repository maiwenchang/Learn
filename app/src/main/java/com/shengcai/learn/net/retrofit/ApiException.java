package com.shengcai.learn.net.retrofit;

import java.io.IOException;

/**
 * 异常类
 * <p>
 * Created by tianyang on 2017/9/27.
 */

public class ApiException extends IOException {
    public ApiException(String message) {
        super(message);
    }
}
