package com.shengcai.learn.net.base;

import android.support.annotation.NonNull;

import com.shengcai.learn.net.retrofit.NetMgr;
import com.shengcai.learn.util.Logger;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Created by MaiWenChang on 2017/10/13.
 */

public class ApiBaseService<T> {

    public T ApiClient(String baseUrl) {
        return NetMgr.getInstance().getRetrofit(baseUrl).create(getType());
    }


//    //指定观察者与被观察者线程
//    public <T> Observable.Transformer<T, T> normalSchedulers() {
//        return new Observable.Transformer<T, T>() {
//            @Override
//            public Observable<T> call(Observable<T> source) {
//                return source.onTerminateDetach().subscribeOn(Schedulers.io())
//                        .observeOn(AndroidSchedulers.mainThread());
//            }
//        };
//    }

    private Class<T> getType() {
        Class<T> entityClass = null;
        Type t = getClass().getGenericSuperclass();
        Type[] p = ((ParameterizedType) t).getActualTypeArguments();
        entityClass = (Class<T>) p[0];
        return entityClass;
    }

}
