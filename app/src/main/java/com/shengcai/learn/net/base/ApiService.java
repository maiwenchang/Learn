package com.shengcai.learn.net.base;

import android.support.annotation.NonNull;

import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;

/**
 * Created by MaiWenChang on 2017/10/13.
 */

public class ApiService extends ApiBaseService<IApi> {

    private static volatile ApiService instance;

    public static ApiService getInstance() {
        if (instance == null) {
            Class var0 = ApiService.class;
            synchronized (ApiService.class) {
                if (instance == null) {
                    instance = new ApiService();
                }
            }
        }

        return instance;
    }


    public Call<ResponseBody> get(@NonNull String BaseUrl, String Url, Map<String, String> params) {
        return ApiClient(BaseUrl).get(Url, params);
    }

    public Call<ResponseBody> post(@NonNull String BaseUrl, String Url, Map<String, String> params) {
        return ApiClient(BaseUrl).post(Url, params);
    }



}
