package com.shengcai.learn.net.base;

import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.QueryMap;
import retrofit2.http.Url;

/**
 * Created by MaiWenChang on 2017/10/13.
 */

public interface IApi {

    @GET()
    Call<ResponseBody> get(@Url String url, @QueryMap Map<String, String> params);


    @POST()
    Call<ResponseBody> post(@Url String url, @QueryMap Map<String, String> params);

}
