package com.shengcai.learn.net.base;

/**
 * Created by MaiWenChang on 2017/11/12.
 */

public class BaseUrl {

    /* 获取城市信息 */
    public static final String guolin = "http://guolin.tech/";

    public static final String www100 = "http://www.100xuexi.com/";

    public static final String app100 = "http://app.100xuexi.com/";

    public static final String service100 = "http://service.100eshu.com/";

    public static final String tk100 = "http://tk.100xuexi.com/";

    public static final String g100 = "http://g.100xuexi.com/";

    public static final String wx100 = "http://wx.100xuexi.com/";

    public static final String zhibo100 = "http://zhibo.100xuexi.com/";



}
