package com.shengcai.learn.module.catalog;

import com.shengcai.learn.module.catalog.Book;

interface IOnNewBookArrivedListener {
    void onNewBookArrived(in Book newBook);
}
