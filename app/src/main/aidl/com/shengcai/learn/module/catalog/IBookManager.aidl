package com.shengcai.learn.module.catalog;

import com.shengcai.learn.module.catalog.Book;
import com.shengcai.learn.module.catalog.IOnNewBookArrivedListener;

interface IBookManager {
     List<Book> getBookList();
     void addBook(in Book book);
     void registerListener(IOnNewBookArrivedListener listener);
     void unregisterListener(IOnNewBookArrivedListener listener);
}