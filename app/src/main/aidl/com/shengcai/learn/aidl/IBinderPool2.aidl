// IBinderPool2.aidl
package com.shengcai.learn.aidl;

// Declare any non-default types here with import statements

interface IBinderPool2 {
    /**
     * Demonstrates some basic types that you can use as parameters
     * and return values in AIDL.
     */
    IBinder queryBinder(int binderCode);
}
