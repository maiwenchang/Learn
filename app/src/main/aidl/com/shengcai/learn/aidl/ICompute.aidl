// ICompute.aidl
package com.shengcai.learn.aidl;

// Declare any non-default types here with import statements

interface ICompute {
    /**
     * Demonstrates some basic types that you can use as parameters
     * and return values in AIDL.
     */
    int factorial(int i);

    int getI();

    void startLoop();
}
